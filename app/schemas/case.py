#!/usr/bin/env python
# -*- coding:utf-8 -*-
# @Time   : 2021/9/7 22:31
# @Author : cjw
from typing import Optional, Union

from pydantic import BaseModel


class CaseBase(BaseModel):
	node_id: Union[str, list]


class CaseCreate(CaseBase):
	remark: Optional[str] = None


class Case(CaseCreate):
	id: int

	class Config:
		orm_mode = True
