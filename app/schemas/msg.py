#!/usr/bin/env python
# -*- coding:utf-8 -*-
# @Time      :2021/9/8 14:19
# @Author    :cjw
from typing import Any, Optional
from pydantic import BaseModel


class Msg(BaseModel):
	code: int
	data: Any
	count: Optional[int] = None
	msg: str
