#!/usr/bin/env python
# -*- coding:utf-8 -*-
# @Time   : 2021/9/25 19:17
# @Author : cjw
from datetime import datetime

from pydantic import BaseModel, HttpUrl


class TaskBase(BaseModel):
    information: str
    report: HttpUrl


class TaskCreate(TaskBase):
    pass


class Task(TaskCreate):
    id: int
    create_at: datetime

    class Config:
        orm_mode = True
