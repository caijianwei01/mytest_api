#!/usr/bin/env python
# -*- coding:utf-8 -*-
# @Time      :2021/7/16 9:59
# @Author    :cjw
"""工具函数"""
import asyncio
import requests


async def async_local_ip() -> str:
    """利用协程获取本地ip地址"""
    loop = asyncio.get_event_loop()
    transport, protocol = await loop.create_datagram_endpoint(
        asyncio.DatagramProtocol,
        remote_addr=('8.8.8.8', 80))
    result = transport.get_extra_info('sockname')[0]
    transport.close()
    return result


def get_local_ip():
    """获取本地ip"""
    return asyncio.run(async_local_ip())


def get_public_ip() -> str:
    """获取公网IP"""
    return requests.get('http://ip.42.pl/raw').text