#!/usr/bin/env python
# -*- coding:utf-8 -*-
# @Time      :2021/9/24 13:59
# @Author    :cjw
import time
from jenkinsapi.jenkins import Jenkins


class ExecuteTools:

	def __init__(self, base_url: str, username: str, token: str):
		"""
		:param base_url: jenkins的服务地址
		:param username: 用户名
		:param token: 连接的token
		"""
		self.base_url = base_url
		self.username = username
		self.token = token
		self.jenkins = Jenkins(self.base_url, self.username, self.token)

	def get_jobs(self):
		"""获取所有的jenkins任务"""
		return self.jenkins.keys()

	def invoke(self, job_name: str, build_params: dict, timeout: int = 10):
		"""
		job构建
		:param job_name: 构建的job名称
		:param build_params: 构建参数
		:param timeout: 超时时间
		:return:
		"""
		if job_name not in self.get_jobs():
			raise ValueError(f'构建的job名称不存在：{job_name}')

		job = self.jenkins.get_job(job_name)
		# 构建job，需要和jenkins的参数名进行对应
		job.invoke(build_params=build_params)
		# 方式1：Jenkins 自动生成Junit.xml 报告，拿到之后解析xml文件，获取用例执行的信息
		# 方式2：拿到allure的数据信息，解析json文件，获取用例的执行
		# 方式3：直接拿到allure的链接，就是allure的报告信息
		# allure链接地址的拼接：http://101.34.44.7:8080/job/pt_test/24/allure/
		last_build_num = job.get_last_buildnumber()
		end_time = time.time() + timeout
		while True:  # 循环获取构建的number，直到获取到最新的
			build_num = job.get_last_buildnumber()
			if build_num != last_build_num:
				break
			if time.time() > end_time:
				raise TimeoutError(f'获取“{job_name}”的最新一次构建number超时')
		allure_url = f'{self.base_url}job/{job_name}/{build_num}/allure/'
		return allure_url
