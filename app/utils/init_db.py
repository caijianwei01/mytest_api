#!/usr/bin/env python
# -*- coding:utf-8 -*-
# @Time      :2021/9/8 10:21
# @Author    :cjw
from app.models.case import Case
from app.models.task import Task
from app.api.utils.db import create_table

if __name__ == '__main__':
	create_table()
