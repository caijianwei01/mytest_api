#!/usr/bin/env python
# -*- coding:utf-8 -*-
# @Time   : 2021/9/25 17:57
# @Author : cjw
from datetime import datetime

from sqlalchemy import Column, String, Integer, DateTime
from app.api.utils.db import Base


class Task(Base):
    __tablename__ = 'tasks'

    id = Column(Integer, primary_key=True)
    information = Column(String(200), nullable=False, comment='任务执行信息')
    report = Column(String(200), nullable=False, comment='执行报告')
    create_at = Column(DateTime, default=datetime.now, comment='执行时间')

    def __repr__(self):
        return f'Task("{self.information}")'

    def as_dict(self):
        return {
            'id': self.id,
            'information': self.information,
            'report': self.report,
            'create_at': self.create_at
        }
