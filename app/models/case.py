#!/usr/bin/env python
# -*- coding:utf-8 -*-
# @Time   : 2021/9/7 22:21
# @Author : cjw
from sqlalchemy import Column, String, Integer
from app.api.utils.db import Base


class Case(Base):
	"""案例信息表"""
	__tablename__ = 'cases'
	id = Column(Integer, primary_key=True, index=True)
	node_id = Column(String(100), nullable=False, comment='节点id')
	remark = Column(String(200), comment='备注信息')

	def __repr__(self):
		return f'Item("{self.node_id}")'

	def as_dict(self):
		"""返回字典数据"""
		return {
			'id': self.id,
			'node_id': self.node_id,
			'remark': self.remark
		}
