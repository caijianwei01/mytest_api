#!/usr/bin/env python
# -*- coding:utf-8 -*-
# @Time   : 2021/9/7 21:47
# @Author : cjw
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from app.api.v1 import case, task

tag_metadata = [
    {
        'name': 'cases',
        'description': '用例的增删改查操作'
    }
]
app = FastAPI(title='自测平台api', openapi_tags=tag_metadata)

# CORS（跨域资源共享）配置, 存在则配置，影响所有接口
# 域的概念：协议+域名+端口
app.add_middleware(
    CORSMiddleware,
    allow_origins=['*'],
    allow_credentials=True,
    allow_methods=['*'],
    allow_headers=['*'],
)

app.include_router(case.router, prefix='/api/v1')
app.include_router(task.router, prefix='/api/v1')

if __name__ == '__main__':
    import uvicorn

    uvicorn.run('main:app', host='0.0.0.0', reload=True, debug=True)
