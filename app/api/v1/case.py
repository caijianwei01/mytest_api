#!/usr/bin/env python
# -*- coding:utf-8 -*-
# @Time      :2021/9/8 9:53
# @Author    :cjw
import json
from typing import Optional

from fastapi import APIRouter, Depends, status, Response
from sqlalchemy.orm import Session
from loguru import logger

from app.crud import case as case_crud
from app.api.utils.dependencies import get_db
from app.models.case import Case
from app.schemas.case import CaseCreate, Case as SCase
from app.schemas.msg import Msg

router = APIRouter(prefix='/case', tags=['用例管理'])


@router.get('', response_model=Msg, summary='获取单个用例信息')
def get_case(response: Response, c_id: Optional[int] = None, node_id: Optional[str] = None,
             db: Session = Depends(get_db)):
	"""
	获取单个用例信息
	:param response:
	:param c_id:
	:param node_id:
	:param db:
	:return:
	"""
	case_data: Optional[Case] = None
	if c_id:
		case_data = case_crud.get_case(db, c_id)
	elif node_id:
		case_data = case_crud.get_case_by_node_id(db, node_id)

	if not case_data:
		response.status_code = status.HTTP_404_NOT_FOUND
		msg = {'code': -1, 'data': f'查询的用例id：{c_id}', 'msg': '用例信息不存在'}
		logger.error(msg)
		return msg
	msg = {'code': 0, 'data': case_data.as_dict(), 'msg': '获取成功'}
	logger.info(msg)
	return msg


@router.get('/page_size', response_model=Msg, summary='分页获取用例')
def get_cases(skip: int = 0, limit: int = 10, db: Session = Depends(get_db)):
	"""
	分页获取
	:param skip:
	:param limit:
	:param db:
	:return:
	"""
	case_datas = case_crud.get_cases(db, skip, limit)
	count = case_crud.get_case_count(db).get('count', 0)
	msg = {'code': 0, 'data': case_datas, 'count': count, 'msg': '获取成功'}
	logger.info(msg)
	return msg


@router.post('/create', response_model=Msg, summary='创建用例')
def create_case(case: CaseCreate, db: Session = Depends(get_db)):
	"""
	创建用例
	:param case:
	:param db:
	:return:
	"""
	# 如果数据字段存在列表，需要做一次转换
	if isinstance(case.node_id, list):
		case.node_id = json.dumps(case.node_id, ensure_ascii=False)
	try:
		case_crud.create_case(db, case)
		msg = {'code': 0, 'data': case.dict(), 'msg': '创建成功'}
		logger.info(msg)
	except Exception as e:
		msg = {'code': -1, 'data': case.dict(), 'msg': '创建失败'}
		logger.info(msg)
		logger.info(f'创建失败原因：{e}')
	return msg


@router.put('/update', response_model=Msg, summary='更新用例信息')
def update_case(case: SCase, response: Response, db: Session = Depends(get_db)):
	"""
	更新用例信息
	- **case**: 用例信息
	- **db**: 数据库连接
	"""
	case_data = case_crud.get_case(db, case.id)
	if not case_data:
		response.status_code = status.HTTP_404_NOT_FOUND
		msg = {'code': -1, 'data': case.dict(), 'msg': '用例信息不存在'}
		logger.info(msg)
		return msg
	try:
		case_crud.update_case(db, case)
		msg = {'code': 0, 'data': case.dict(), 'msg': '更新成功'}
		logger.info(msg)
	except Exception as e:
		msg = {'code': -1, 'data': case.dict(), 'msg': '更新失败'}
		logger.info(msg)
		logger.info(f'更新失败原因：{e}')
	return msg


@router.delete('/delete', response_model=Msg, summary='删除用例信息')
def delete_case(response: Response, *, node_id: Optional[str] = None, c_id: Optional[int] = None,
                db: Session = Depends(get_db)):
	"""
	删除案例信息
	- **c_id**: 用例id
	- **node_id**: 用例id
	- **db**: 数据库连接
	"""
	case_data: Optional[Case] = None
	if c_id:
		case_data = case_crud.get_case(db, c_id)
	elif node_id:
		if isinstance(node_id, list):
			node_id = json.dumps(node_id, ensure_ascii=False)
		case_data = case_crud.get_case_by_node_id(db, node_id)

	if not case_data:
		response.status_code = status.HTTP_404_NOT_FOUND
		msg = {'code': -1, 'data': f'删除的用例id：{c_id} 或者 node_id: {node_id}', 'msg': '用例信息不存在'}
		logger.info(msg)
	try:
		# 判断是通过id还是node_id删除
		if c_id:
			case_crud.delete_case(db, c_id)
		else:
			case_crud.delete_case(db, node_id=node_id)
		msg = {'code': 0, 'data': case_data.as_dict(), 'msg': '删除成功'}
		logger.info(msg)
	except Exception as e:
		msg = {'code': -1, 'data': case_data.as_dict(), 'msg': '删除失败'}
		logger.info(msg)
		logger.info(f'删除失败原因：{e}')
	return msg
