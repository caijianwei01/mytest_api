#!/usr/bin/env python
# -*- coding:utf-8 -*-
# @Time   : 2021/9/7 21:58
# @Author : cjw
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm.session import sessionmaker

# 创建引擎,准备连接，future：v2.0 API版本风格
engine = create_engine(
    'mariadb+pymysql://root:123456@101.34.44.7:3306/mytest',
    echo=True,
    future=True
)

# 在SQLAlchemy中，CRUD都是通过会话(session)进行的，所以我们必须要先创建会话，每一个SessionLocal实例就是一个数据库session
# flush(): 指发送数据库语句到数据库，但数据库不一定执行写入磁盘
# commit(): 是指提交事务，将变更保存到数据库文件中
# 数据库连接, 使用2.0版本的Api风格
SessionLocal = sessionmaker(bind=engine, future=True)

# 声明ORM模型的基类
Base = declarative_base()


def create_table():
    """创建数据库表"""
    Base.metadata.create_all(bind=engine)


def drop_table():
    """删除所有数据库表"""
    Base.metadata.drop_all(bind=engine)
