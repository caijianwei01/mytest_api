#!/usr/bin/env python
# -*- coding:utf-8 -*-
# @Time   : 2021/9/7 21:47
# @Author : cjw
from sqlalchemy.orm import Session
from app.api.utils.db import SessionLocal


def get_db():
    """生成数据库连接"""
    db: Session = SessionLocal()
    try:
        yield db
    finally:
        db.close()