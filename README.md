# 自测平台api

***

**创建虚拟环境(默认使用系统的版本，也可以指定版本)：**

1. 进入项目根目录下，创建虚拟环境执行命令：virtualenv venv(虚拟环境名称)
2. 进入目录：cd venv/Scripts， 激活虚拟环境：activate.bat 文件路径最前面显示(venv)，那么虚拟环境就激活成功了
3. 更新pip: python.exe -m pip install --upgrade pip
4. 切换到项目根目录：cd ../../, 安装所有需要的包：pip install -r requirements.txt
5. 查看虚拟环境安装的所有包：pip list

**pip常用命令使用：**

* 导出虚拟环境安装包：pip freeze  > requirements.txt
* 在其它虚拟环境安装requirements.txt文件中的包： pip install -r requirements.txt

***