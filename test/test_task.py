#!/usr/bin/env python
# -*- coding:utf-8 -*-
# @Time   : 2021/9/25 15:07
# @Author : cjw
from fastapi.testclient import TestClient

from app.main import app


class TestTask:
    def setup_class(self):
        self.client = TestClient(app)

    def test_get_tasks(self):
        resp = self.client.get('/api/v1/task/page_size')
        assert resp.status_code == 200

    def test_create_task(self):
        # 用例的信息
        payload = {
            'id': '123',
            'node_id': 'test_calculator.py'
        }
        resp = self.client.post('/api/v1/task/create', json=payload)
        assert resp.status_code == 200
