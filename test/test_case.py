#!/usr/bin/env python
# -*- coding:utf-8 -*-
# @Time      :2021/9/8 14:43
# @Author    :cjw
import json
import time
from fastapi.testclient import TestClient

from app.main import app


class TestCase:
	def setup_class(self):
		self.client = TestClient(app)

	def test_get_case(self):
		"""获取用例信息"""
		resp = self.client.get('/api/v1/case', params={'c_id': 1})
		assert resp.status_code == 200
		assert resp.json()['code'] == 0

	def test_get_case_by_node_id(self):
		"""根据用例的节点id获取信息"""
		resp = self.client.get('/api/v1/case', params={'node_id': '自定义节点'})
		assert resp.status_code == 200
		assert resp.json()['code'] == 0

	def test_get_cases(self):
		"""分页获取"""
		resp = self.client.get('/api/v1/case/page_size')
		assert resp.status_code == 200
		assert resp.json()['code'] == 0

	def test_create_case(self):
		"""新增用例数据"""
		payload = {
			'node_id': [f'测试用例{round(time.time() * 1000000)}' for _ in range(2)],
			'remark': f'备注{round(time.time() * 1000)}'
		}
		resp = self.client.post('/api/v1/case/create', json=payload)
		assert resp.status_code == 200
		assert resp.json()['code'] == 0

		# 删除数据
		self.client.delete('/api/v1/case/delete', params={'node_id': json.dumps(payload['node_id'],ensure_ascii=False)})

	def test_update_case(self):
		"""更新用例数据"""
		payload = {
			'node_id': f'测试用例{round(time.time() * 1000)}',
			'remark': f'备注{round(time.time() * 1000)}'
		}
		self.client.post('/api/v1/case/create', json=payload)
		rsp = self.client.get('/api/v1/case', params={'node_id': payload['node_id']})
		payload_update = {
			'id': rsp.json()['data']['id'],
			'node_id': f'测试用例{round(time.time() * 1000)}',
			'remark': f'备注{round(time.time() * 1000)}'
		}
		resp = self.client.put('/api/v1/case/update', json=payload_update)
		assert resp.status_code == 200
		assert resp.json()['code'] == 0

		self.client.delete('/api/v1/case/delete', params={'c_id': payload_update['node_id']})

	def test_delete_case(self):
		"""删除用例数据"""
		payload = {
			'node_id': f'测试用例{round(time.time() * 1000)}',
			'remark': f'备注{round(time.time() * 1000)}'
		}
		self.client.post('/api/v1/case/create', json=payload)

		resp = self.client.delete('/api/v1/case/delete', params={'node_id': payload['node_id']})
		assert resp.status_code == 200
		assert resp.json()['code'] == 0
