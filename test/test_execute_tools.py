#!/usr/bin/env python
# -*- coding:utf-8 -*-
# @Time      :2021/9/24 14:05
# @Author    :cjw
from app.utils.execute_tools import ExecuteTools


class TestExecuteTools:

	def setup_class(self):
		self.execute = ExecuteTools('http://101.34.44.7:8080/', 'cjw', '1128e80f11866f94ecb7cf0fa7ec6cddb9')

	def test_get_jobs(self):
		assert 'selenium_python' in self.execute.get_jobs()

	def test_invoke(self):
		url = self.execute.invoke('pt_test', {'task': 'test task'})
		print(url)
